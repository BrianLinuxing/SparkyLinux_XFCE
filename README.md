# SparkyLinux_XFCE

This repo contains various scripts relating to the setup of an SparkyLinux_XFCE installation, or at least the way I do it. 

I like setting up machines in a predictable and scripted way.

Enjoy, but use with discretion.

BrianLinuxing

PS: I am not connected to the SparkyLinux project, just a contented user. 

Their site is here: https://sparkylinux.org/