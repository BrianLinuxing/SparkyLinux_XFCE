set -o errexit
set -v
#!/bin/sh
#
# Licence: MIT
#
#-----------------------------------------------------------------------------
#Purpose:
#--------
# 1. Set up a standard Ubuntu/Debian based system with the required GUI apps.
# 2. A rather quick/dirty way of updating packages, post an initial installation.
# 3. There have been various incarnations since about 2008, or before.
# 4. This is a smaller selection than in previous versions due to space
# limitations and slower processor speeds.
#
#Author(s): B.Byrne.
#
#Date: July 2023
#
#Distro: Sparky - XFCE, July 2023 version. Kernel of 6.3.0-1-amd64 
#
#-----------------------------------------------------------------------------
#Notes:
#------
#
# 1. This code is now distro specific, i.e. Debian vs. Ubuntu vs. RHEL types to
# avoid minor repo errors. Currently, based on my Diet Pi example. Again,
# material taken from my LXLE setup on a 2006 iMac (May 2020).
#
# 2. It makes no assumptions about pre-existing packages that might already
# have been installed. But does assume the installation has been done correctly,
# along with an update/upgrade and nothing much more.
#
# 3. This version includes extra GUI tools and a build tool section, for the
# sake of completeness. And is formatted, mostly, to a 80 characters width.
#
# 4. Make it executable, first.
#
# 5. Best to execute either via root directly, or with tee -a via sudo and with
# a time command too.
# e.g. time sudo ./apt-packages_Sparky_XFCE_short_v1k.sh | tee -a install_packages_$(date '+%Y_%m_%d_%H_%M_%S')_$(hostname).txt
#
#-----------------------------------------------------------------------------
#
#TDB: exit code, more debugging options and timing functions.
#
#-----------------------------------------------------------------------------
#
apt update
#
apt-get -y -V install apt-transport-https apt-utils apt-listchanges
#
apt-get -y -V install automake curl
#
apt-get -y -V install ddrescueview gddrescue
#
apt-get -y -V install debian-goodies lsb-release
#
apt-get -y -V install fakeroot
#
apt-get -y -V install pcmanfm libfm-tools
#
apt-get -y -V install gparted ntfs-3g mtools dosfstools partclone gpart udftools
#
apt-get -y -V install gdebi git git-gui gitk gitweb
#
apt-get -y -V install grsync rsync
#
apt-get -y -V install hardinfo inxi iotop needrestart
#
apt-get -y -V install lshw lsof lxterminal lynis lnav
#
apt-get -y -V install mc make moreutils mtr
#
apt-get -y -V install nano net-tools ncdu nmon nnn
#
apt-get -y -V install pluma retext
#
apt-get -y -V install screen smartmontools smem ssh-askpass
#
apt-get -y -V install thunar ttyrec tree trash-cli
#
apt-get -y -V install xarchiver unzip arj lhasa lrzip lzip lzop ncompress zstd zip bzip2 bzr p7zip-full xz-utils
#
apt-get -y -V install virtualenv wget whois nmap
#
#-----------------------------------------------------------------------------
# GUI
#
apt-get -y  -V install deluge
#
apt-get -y  -V install filezilla gthumb
#
apt-get -y  -V install fonts-courier-prime fonts-dejavu fonts-freefont-ttf
#
apt-get -y  -V install fonts-liberation fonts-liberation2 fonts-linuxlibertine
#
apt-get -y  -V install fonts-opendyslexic fonts-open-sans fonts-opensymbol
#
apt-get -y  -V install fonts-ubuntu fonts-ubuntu-console
#
apt-get -y  -V install fonts-wine
#
apt-get -y  -V install geany*
#
apt-get -y  -V install hunspell-en-gb hunspell
#
apt-get -y  -V install meld
#
apt-get -y  -V install okular
#
apt-get -y  -V install yt-dlp media-downloader celluloid
#
#-----------------------------------------------------------------------------
# Build tools
#
apt-get -y -V install python3 gettext
#
apt-get -y -V install python3-pip
#
apt-get -y -V install python3-spyder python3-openssl flake8 python3-pycurl
#
apt-get -y -V install python3-brotli libbrotli-dev libbz2-dev
#
apt-get -y -V install libffi-dev libreadline-dev
#
apt-get -y -V install uncrustify
#
apt-get -y -V install pkg-config
#
apt-get -y -V install cmake build-essential gcc-arm-none-eabi libnewlib-arm-none-eabi
#
#-----------------------------------------------------------------------------
# Fixes
#
apt-get -y -V install default-jre liblibreoffice-java libreoffice-java-common libunoil-java libreoffice-gtk3
#
#-----------------------------------------------------------------------------
exit